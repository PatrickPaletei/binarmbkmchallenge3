package id.ac.ukdw.challenge3new

data class DbWord(val key:String,val value:String)

object WordList{
    var wordList = listOf(DbWord("A","ANGSA"),DbWord("A","ANGGORA"),DbWord("A","ANDONG")
        ,DbWord("B","BOLA"),DbWord("B","BERANI"),DbWord("B","BERKUDA"),
        DbWord("C","CANTIK"),DbWord("C","CICAK"),DbWord("C","CUMI-CUMI"),
        DbWord("D","DUKUN"),DbWord("D","DOSEN"),DbWord("D","DOMBA"),
        DbWord("E","EASTER"),DbWord("E","EDEN HAZARD"),DbWord("E","EMTEK"),
        DbWord("F","FIFA"),DbWord("F","FILM BARU"),DbWord("F","FRUTANG"),
        DbWord("G","GORENGAN"),DbWord("G","GALAK"),DbWord("G","GALAU"),
        DbWord("H","EASTER"),DbWord("H","EDEN HAZARD"),DbWord("H","EMTEK"),
        DbWord("I","FIFA"),DbWord("I","FILM BARU"),DbWord("I","FRUTANG"),
        DbWord("J","JEROAN"),DbWord("J","JARING"),DbWord("J","JUMAT")
    )
}
