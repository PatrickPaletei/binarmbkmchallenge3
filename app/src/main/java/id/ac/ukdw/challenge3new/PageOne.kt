package id.ac.ukdw.challenge3new

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.ac.ukdw.challenge3new.databinding.FragmentPageOneBinding
import id.ac.ukdw.challenge3new.databinding.FragmentPageTwoBinding


class PageOne : Fragment(),MyAdapter.onItemClickListener {

    private lateinit var adapter: MyAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var wordArrayList: ArrayList<Word>
    private var _binding:FragmentPageOneBinding?=null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPageOneBinding.inflate(inflater, container, false)

        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = "Words"
        (activity as MainActivity).hideUpButton()
        dataInit()
        val layoutManager = LinearLayoutManager(context)
        recyclerView = view.findViewById(R.id.rcyView)
        recyclerView.layoutManager= layoutManager
        recyclerView.setHasFixedSize(true)
        adapter = MyAdapter(wordArrayList,this)
        recyclerView.adapter = adapter

    }


    private fun dataInit(){
        wordArrayList = arrayListOf<Word>(Word("A"),Word("B"),Word("C")
            ,Word("D"),Word("E"),Word("F")
        ,Word("G"),Word("H"),Word("I"),Word("J"))

    }

    override fun onItemClick(position: Int,word:String) {
        val bundle = Bundle()
        bundle.putString("positionWord",word)
        val fragmentManager = parentFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val pageTwo = PageTwo()
        pageTwo.arguments = bundle
        fragmentTransaction.replace(R.id.frameLayout,pageTwo)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
//        Toast.makeText(context, "test", Toast.LENGTH_LONG).show()
    }

//    override fun passData(word: String) {
//        val bundle = Bundle()
//        bundle.putString("word",word)
//        val activity=v.context as AppCompatActivity
//        val transaction = activity.supportFragmentManager.beginTransaction()
//        val pageTwo = PageTwo()
//        pageTwo.arguments = bundle
//        transaction.replace(R.id.frameLayout,pageTwo)
//        transaction.commit()
//    }


}