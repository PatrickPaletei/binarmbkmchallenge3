package id.ac.ukdw.challenge3new

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.sparkmuse.OxfordClient
import com.github.sparkmuse.entity.RetrieveEntry
import id.ac.ukdw.challenge3new.databinding.FragmentPageTwoBinding

class PageTwo : Fragment(),MyAdapter2.onItemClickListener {


    private var _binding:FragmentPageTwoBinding? = null
    private val binding get()=_binding!!
//    private lateinit var DbwordArrayList: ArrayList<DbWord>
    private lateinit var adapter: MyAdapter2
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding=FragmentPageTwoBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showUpButton()
        (activity as MainActivity).clickUpButton()

        val word = arguments?.getString("positionWord").toString()
//        binding.textView.text = word
        val res = WordList.wordList.filterByWord(word)
        val layoutManager = LinearLayoutManager(context)
        recyclerView = view.findViewById(R.id.rcyView2)
        recyclerView.layoutManager= layoutManager
        recyclerView.setHasFixedSize(true)
        adapter = MyAdapter2(res,this)
        recyclerView.adapter = adapter

    }

//    fun List<DbWord>.filterByWord(value:String,key:String)=this.filter { it.value.startsWith(key) }
    fun List<DbWord>.filterByWord(key:String)=
        this.filter {
            it.value.startsWith(key)
        }

    override fun onItemClick(position: Int, word: String) {
        openWeb(word)
    }
    fun openWeb(word: String){
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/search?q=$word"))
        startActivity(intent)
    }


}