package id.ac.ukdw.challenge3new

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapter2(private val wordDb:List<DbWord>,val mListener2: onItemClickListener): RecyclerView.Adapter<MyAdapter2.MyViewHolder>() {

    interface onItemClickListener{
        fun onItemClick(position: Int,word:String)
    }

    class MyViewHolder(itemView: View, mListener: onItemClickListener):RecyclerView.ViewHolder(itemView){
        val word = itemView.findViewById<TextView>(R.id.btnWord)

        init {
            itemView.setOnClickListener {
                mListener.onItemClick(adapterPosition, word.text.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)
        return MyViewHolder(view,mListener2)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.word.text=wordDb[position].value
    }

    override fun getItemCount(): Int {
        return wordDb.size
    }
}