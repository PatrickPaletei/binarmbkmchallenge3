package id.ac.ukdw.challenge3new

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.core.view.MenuProvider
import androidx.fragment.app.commit
import id.ac.ukdw.challenge3new.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val fragmentManager = supportFragmentManager
//        val fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.add(R.id.frameLayout,PageOne())
//        fragmentTransaction.commit()

        fragmentManager.commit{
            add(R.id.frameLayout,PageOne())
        }

    }

    fun showUpButton(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun hideUpButton(){
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    fun clickUpButton(){
        addMenuProvider(object :MenuProvider{
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                return
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when(menuItem.itemId){
                    android.R.id.home ->{
                        supportFragmentManager.popBackStack()
                        true
                    }
                    else -> false
                }
            }
        })

}}