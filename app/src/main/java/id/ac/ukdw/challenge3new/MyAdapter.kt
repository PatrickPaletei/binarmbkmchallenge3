package id.ac.ukdw.challenge3new

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import id.ac.ukdw.challenge3new.databinding.ListItemBinding


class MyAdapter(private val wordList:ArrayList<Word>,val mListener:onItemClickListener):RecyclerView.Adapter<MyAdapter.MyViewHolder>() {


    interface onItemClickListener{
        fun onItemClick(position: Int,word:String)
    }



    class MyViewHolder(itemView: View,mListener:onItemClickListener):RecyclerView.ViewHolder(itemView){
        val word = itemView.findViewById<TextView>(R.id.btnWord)

        init {
            itemView.setOnClickListener {
                mListener.onItemClick(adapterPosition,word.text.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item,parent,false)
        return MyViewHolder(view,mListener)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.word.text = wordList[position].word
    }

    override fun getItemCount(): Int {
        return wordList.size
    }
}